# Mastering State in React
### For project run
```
$ npm start
```
### For server run
```
$ npm run start
```
___
## Extension
- [Craco](https://www.npmjs.com/package/@craco/craco) was used to change the webpack ```devServer``` configuration. (craco.config.js and .env files were created).
- For the server, use the ```personal-website-server``` which you should have already forked for the ["Using the Fetch API"](https://gitlab.com/iks3000/ajax) task.

___
## Task description
- When the page has loaded, implement the functionality of displaying information in the section "Big Community of People Like You" based on the design in Figma. To implement it, make a GET Ajax request using the ```/community``` endpoint.
- The subscribe form in the **"Join Our Program"** section should be connected to the server.
- Implement the functionality of sending a user's email to the server when they click on the **"Subscribe"** button. To do this, make a POST Ajax request using the ```/subscribe``` endpoint.
- When email is ```forbidden@gmail.com```, the server should respond with the ```422``` status code and a payload containing information about the error - ```{ "error": "Email is already in use" }```. Use browser developer tools to examine the response to this scenario. Display the error message in the browser using ```window.alert()```.
- Implement the functionality of unsubscribing the user's email from the community list when they click on the **"Unsubscribe"** button. To do this, make a POST Ajax request using the ```/unsubscribe``` endpoint.
- Prevent additional requests if the **"Subscribe"** and **"Unsubscribe"** buttons are pressed while requests to the ```/subscribe``` and ```/unsubscribe``` endpoints are in progress. Also, disable them (using the disabled attribute) and style them using ```opacity: 0.5```.
- Add a button to toggle the visibility of the section content (the area inside the blue rectangle in the picture below). For the open state, the button text should be "Hide section". For the hidden state, it should be "Show section".