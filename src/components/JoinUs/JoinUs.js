import React, { useState } from "react";
import './JoinUs.scss';

const VALID_EMAIL_ENDINGS = ["gmail.com", "outlook.com", "yandex.ru"];

const validate = (email) => {
    const inputEnding = email.substring(
        email.indexOf("@") + 1
    );
    return VALID_EMAIL_ENDINGS.some((value) => value === inputEnding);
}

const JoinUs = () => {
    const [inputVal, setInputVal] = useState('');
    const [isDisabled, setDisabled] = useState(false);
    const [opacity, setOpacity] = useState(1);
    const [visible, setVisible] = useState(true);

    let dataEmail = { email: inputVal };

    const clickSubscride = (e) => {
        e.preventDefault();
        if (inputVal === "") {
            console.log(inputVal)
            window.alert("Type Email address")
        } else if (!validate(inputVal)) {
            window.alert("Invalid email, only gmail.com, outlook.com and yandex.ru");
            setInputVal('');
        } else {
            setDisabled(true);
            setOpacity(0.5);
            fetch('/subscribe', {
                method: "POST",
                headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
                body: JSON.stringify(dataEmail)
            })
            .then((response) => {
                if (response.status === 200) {
                    setDisabled(false);
                    setOpacity(1);
                    setInputVal('');
                    setVisible(false);
                    console.log('response', response)
                    return response.json();
                } else if (response.status === 422) {
                    setDisabled(false);
                    setOpacity(1);
                    setInputVal('');
                    window.alert('Email is already in use');
                }
                return Promise.reject(response);
            })
            .catch((error) => {
                console.log('Something went wrong!', error);
            });
        }
    };

    const clickUnsubscribe = () => {
        setDisabled(true);
        setOpacity(0.5);
        fetch('/unsubscribe', {
            method: "POST",
            headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
        })
            .then(response => {
                if (response.ok === true) {
                    setDisabled(false);
                    setOpacity(1);
                    setVisible(true);
                    return response.json();
                }
            })
            .catch((error) => {
                console.log('Something went wrong!', error);
            });
    }

    return (
        <section className="join-our-program">
            <div className="wrapper">
                <h2 className="join-our-program__title">Join Our Program</h2>
                <p className="join-our-program__sub-title">Sed do eiusmod tempor incididunt<br /> ut labore et dolore magna aliqua. </p>
                {visible ? (
                    <form className="form">
                        <input
                            value={inputVal}
                            onChange={e => setInputVal(e.target.value)}
                            className="subscribe-input"
                            type="email"
                            placeholder="Email"
                        />
                        <button
                            className="subscribe-btn"
                            type="submit"
                            style={{opacity}}
                            onClick={clickSubscride}
                            disabled={isDisabled}
                        >Subscribe
                        </button>
                    </form>
                ) : (
                    <button
                        className="subscribe-btn unsubscribe-btn"
                        style={{ opacity }}
                        onClick={clickUnsubscribe}
                    >Unsubscribe
                    </button>
                )}
            </div>
        </section>
    )
};

export default JoinUs;