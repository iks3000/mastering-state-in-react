import React, { useState, useEffect } from "react";

import './GetUsers.scss';

const GetUsers = () => {
    const [data, setData] = useState(null);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);
    const [isShow, setIsShow] = useState(true);

    const handleClick = () => setIsShow(!isShow);

    useEffect(() => {
        setLoading(true);
        fetch("/community")
            .then((response) => response.json())
            .then(setData)
            .then(() => setLoading(false))
            .catch(setError);
    }, []);

    const UserTemplate = ({ dataUrl }) => {

        const description = [
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolor.",
            "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut.",
            "Aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
        ];

        for (let i = 0, j = 0; i < dataUrl.length; i++, j++) {
            dataUrl[i].description = description[j];
            if (j === description.length - 1) {
                j = -1;
            }
        };

        return (
            <div className="wrapper">
                <div className="hide-warpper">
                    <h2 className="title">Big Community of<br /> People Like You</h2>
                    <button onClick={handleClick} className="btn-hide-show">
                        {isShow ? "Hide section" : "Show section"}
                    </button>
                </div>
                {isShow &&
                    <>
                        <p className="description">We’re proud of our products, and we’re really excited<br /> when we get feedback from our users.</p>
                        <div className="get-user__users-container">
                            {dataUrl.map((dataList, index) => (
                                <div key={index}>
                                    <div className="user">
                                        <img className="user-img" src={dataList.avatar} alt="img" />
                                        <p className="user-description">{dataList.description}</p>
                                        <p className="user-name">{dataList.firstName} {dataList.lastName}</p>
                                        <p className="user-position">{dataList.position}</p>
                                    </div>
                                </div>
                            ))}
                        </div>
                    </>
                }
            </div>
        )
    }

    return (
        <section className="get-user">
            {error && <pre>{JSON.stringify(error, null, 2)}</pre>}
            {loading && <h1>Loading...</h1>}
            {data && <UserTemplate dataUrl={data} />}
        </section>
    )
}

export default GetUsers;
